#include "audio.h"

namespace sokaban {

	Sound click;
	Sound newlevel;
	Sound movebox;
	Sound playermove;

	Sound wingame_s;

	Music menu_song;
	Music gameplay_music;

	namespace audioftg {
		void loadAudio() {

			InitAudioDevice();
			click = LoadSound("res/assets/blip.wav");
			wingame_s = LoadSound("res/assets/wingame.wav");

			newlevel = LoadSound("res/assets/newlevel.wav");
			playermove = LoadSound("res/assets/playermove.wav");
			movebox = LoadSound("res/assets/movebox.wav");
			menu_song = LoadMusicStream("res/assets/Child's Nightmare.ogg");
			gameplay_music = LoadMusicStream("res/assets/Solve The Puzzle.ogg");

			PlayMusicStream(menu_song);

			SetMusicVolume(menu_song, static_cast<float>(0.40));

		}
		void unloadAudio() {

			UnloadSound(newlevel);
			UnloadSound(click);
			UnloadSound(movebox);
			UnloadSound(playermove);

			UnloadMusicStream(menu_song);
			UnloadMusicStream(gameplay_music);

			CloseAudioDevice();

		}
	}
}