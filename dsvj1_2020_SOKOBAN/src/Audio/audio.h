#ifndef AUDIO_H
#define AUDIO_H

#include "raylib.h"

namespace sokaban{

	extern Sound click;
	extern Sound newlevel;
	extern Sound movebox;
	extern Sound playermove;

	extern Sound wingame_s;
	extern Music menu_song;
	extern Music gameplay_music;


	namespace audioftg {

		void loadAudio();
		void unloadAudio();
	}
}
#endif