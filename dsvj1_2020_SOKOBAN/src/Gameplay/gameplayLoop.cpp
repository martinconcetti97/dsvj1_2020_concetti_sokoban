#include "gameplayLoop.h"

#include "gameLoop.h"
#include "gameover.h"
#include "textures.h"
#include "levels.h"
#include "audio.h"

using namespace sokaban;
using namespace gameplay;

namespace sokaban {
	bool gameover, pause;
	buttonplay return_from_g;
	int boxcounter, boxcounteraux;

	namespace gameplay {
		void init() {
			//button
			return_from_g.frameHeight = texture_button.height / NUM_FRAMES;
			return_from_g.sourceRec = { 0, 0, static_cast<float>(texture_button.width), static_cast<float>(return_from_g.frameHeight) };
			return_from_g.btnBounds = { static_cast<float>(GetScreenWidth() / 2 - texture_button.width / 2),
					static_cast<float>(GetScreenHeight() / 1 - 40 - texture_button.height / NUM_FRAMES / 2),  static_cast<float>(texture_button.width), static_cast<float>(return_from_g.frameHeight) };
			return_from_g.btnAction = false;
			return_from_g.btnState = 0;

			return_from_g.mousePoint = { 0.0f, 0.0f };

			//gameplay
			gameover = false;
			pause = false;
			boxcounter = 0;
			ns_levels::init();


		}
		void input() {
			if (pause) {
				return_from_g.mousePoint = GetMousePosition();
				return_from_g.btnAction = false;
				if (CheckCollisionPointRec(return_from_g.mousePoint, return_from_g.btnBounds))
				{
					if (IsMouseButtonDown(MOUSE_LEFT_BUTTON)) {

						return_from_g.btnState = 2;
						PlaySound(click);
						sokaban::level = 1;
						screen = 0;



					}
					else {
						return_from_g.btnState = 1;

					}

				}
				else {
					return_from_g.btnState = 0;
				}
				return_from_g.sourceRec.y = static_cast<float>(return_from_g.btnState * return_from_g.frameHeight);

			}
		}
		void update() {
			if (IsKeyPressed('M')) {
				StopMusicStream(gameplay_music);
				muteMusicbool = !muteMusicbool;
			}
			if (IsKeyPressed('P')) {
				pause = !pause;
				StopMusicStream(gameplay_music);

			}
			if (!pause) {



				if (!muteMusicbool)
				{
					UpdateMusicStream(gameplay_music);
					SetMusicVolume(gameplay_music, static_cast<float>(0.15));
				}

				for (int i = 0; i < ROWS; i++)
				{
					for (int j = 0; j < COLUMNS; j++)
					{

						if (p[i][j].type == 3 && p[i][j].click_2 == true) {
							if (boxcounteraux == 0) {
								boxcounter++;
								boxcounteraux++;

							}

						}
					}
				}
				if (IsKeyPressed(KEY_R)) {
					gameplay::init();
				}
				if (gameover) {
					gameOver::input();
				}
				if (!gameover) {
					ns_levels::update();
				}
				if (level == 1 && boxcounter == 3) {
					stars_plvl[0] = move_counter;
					if (stars_plvl[0] <= 77) {
						cantstars_plvl[0] = 3;
						std::cout << "3";
					}
					else if (stars_plvl[0] > 77 && stars_plvl[0] <= 85) {
						cantstars_plvl[0] = 2;
						std::cout << "2";

					}
					else if (stars_plvl[0] > 85) {
						cantstars_plvl[0] = 1;

						std::cout << "1";
					}
					level = 2;
					PlaySound(newlevel);
					gameplay::init();
				}
				else if (level == 2 && boxcounter == 4) {
					stars_plvl[1] = move_counter;
					if (stars_plvl[1] <= 37) {
						cantstars_plvl[1] = 3;

						std::cout << "3";
					}
					else if (stars_plvl[1] > 37 && stars_plvl[0] <= 45) {
						cantstars_plvl[1] = 2;
						std::cout << "2";

					}
					else if (stars_plvl[1] > 45) {
						cantstars_plvl[1] = 1;
						std::cout << "1";

					}
					level = 3;
					PlaySound(newlevel);

					gameplay::init();
				}
				else if (level == 3 && boxcounter == 4) {
					stars_plvl[2] = move_counter;
					if (stars_plvl[2] <= 61) {
						cantstars_plvl[2] = 3;
						std::cout << "3";

					}
					else if (stars_plvl[2] > 61 && stars_plvl[0] <= 69) {
						cantstars_plvl[2] = 2;
						std::cout << "2";


					}
					else if (stars_plvl[2] > 69) {
						cantstars_plvl[2] = 1;
						std::cout << "1";


					}
					level = 4;
					PlaySound(newlevel);

					gameplay::init();
				}
				else if (level == 4 && boxcounter == 5) {
					stars_plvl[3] = move_counter;
					if (stars_plvl[3] <= 93) {
						cantstars_plvl[3] = 3;

					}
					else if (stars_plvl[3] > 93 && stars_plvl[0] <= 100) {
						cantstars_plvl[3] = 2;


					}
					else if (stars_plvl[3] > 100) {
						cantstars_plvl[3] = 1;


					}
					level = 5;
					PlaySound(newlevel);

					gameplay::init();
				}
				else if (level == 5 && boxcounter == 6) {
					stars_plvl[4] = move_counter;
					if (stars_plvl[4] <= 77) {
						cantstars_plvl[4] = 3;

					}
					else if (stars_plvl[4] > 77 && stars_plvl[0] <= 85) {
						cantstars_plvl[4] = 2;


					}
					else if (stars_plvl[4] > 85) {
						cantstars_plvl[4] = 1;


					}
					gameover = true;
				}
			}
			gameplay::input();

		}
		void draw() {
			DrawTexture(texture_background, GetScreenWidth() / 2 - texture_background.width / 2, GetScreenHeight() / 2 - texture_background.height / 2, WHITE);

			ns_levels::draw();
			if (pause) {
				DrawText("GAME PAUSED", GetScreenWidth() / 2 - (MeasureText("GAME PAUSED", letterSize) / 2), GetScreenHeight() / 2 - letterSize, letterSize, BLACK);
				DrawTextureRec(texture_return, return_from_g.sourceRec, { return_from_g.btnBounds.x, return_from_g.btnBounds.y }, WHITE);
			}
			if (level == 1) {

				DrawText("Level 1", 5, 4, 20, DARKBLUE);
			}
			if (level == 2) {

				DrawText("Level 2", 5, 4, 20, DARKBLUE);

				DrawText("Level 1 stars", 5, GetScreenWidth() / 2 - 20, 20, BLACK);

				if (cantstars_plvl[0] == 3) {
					DrawTexture(texture_star, 5, GetScreenWidth() / 2, WHITE);
					DrawTexture(texture_star, 40, GetScreenWidth() / 2, WHITE);
					DrawTexture(texture_star, 75, GetScreenWidth() / 2, WHITE);

				}
				else if (cantstars_plvl[0] == 2) {

					DrawTexture(texture_star, 5, GetScreenWidth() / 2, WHITE);
					DrawTexture(texture_star, 40, GetScreenWidth() / 2, WHITE);
					DrawTexture(texture_starb, 75, GetScreenWidth() / 2, WHITE);

				}
				else if (cantstars_plvl[0] == 1) {
					DrawTexture(texture_star, 5, GetScreenWidth() / 2, WHITE);
					DrawTexture(texture_starb, 40, GetScreenWidth() / 2, WHITE);
					DrawTexture(texture_starb, 75, GetScreenWidth() / 2, WHITE);
				}
				else {

					DrawTexture(texture_starb, 5, GetScreenWidth() / 2, WHITE);
					DrawTexture(texture_starb, 40, GetScreenWidth() / 2, WHITE);
					DrawTexture(texture_starb, 75, GetScreenWidth() / 2, WHITE);
				}

			}
			if (level == 3) {

				DrawText("Level 3", 5, 4, 20, DARKBLUE);
				DrawText("Level 2 stars", 5, GetScreenWidth() / 2 - 20, 20, BLACK);

				if (cantstars_plvl[1] == 3) {
					DrawTexture(texture_star, 5, GetScreenWidth() / 2, WHITE);
					DrawTexture(texture_star, 40, GetScreenWidth() / 2, WHITE);
					DrawTexture(texture_star, 75, GetScreenWidth() / 2, WHITE);

				}
				else if (cantstars_plvl[1] == 2) {

					DrawTexture(texture_star, 5, GetScreenWidth() / 2, WHITE);
					DrawTexture(texture_star, 40, GetScreenWidth() / 2, WHITE);
					DrawTexture(texture_starb, 75, GetScreenWidth() / 2, WHITE);

				}
				else if (cantstars_plvl[1] == 1) {
					DrawTexture(texture_star, 5, GetScreenWidth() / 2, WHITE);
					DrawTexture(texture_starb, 40, GetScreenWidth() / 2, WHITE);
					DrawTexture(texture_starb, 75, GetScreenWidth() / 2, WHITE);
				}
				else {

					DrawTexture(texture_starb, 5, GetScreenWidth() / 2, WHITE);
					DrawTexture(texture_starb, 40, GetScreenWidth() / 2, WHITE);
					DrawTexture(texture_starb, 75, GetScreenWidth() / 2, WHITE);
				}
			}
			if (level == 4) {

				DrawText("Level 4", 5, 4, 20, DARKBLUE);
				DrawText("Level 3 stars", 5, GetScreenWidth() / 2 - 20, 20, BLACK);

				if (cantstars_plvl[2] == 3) {
					DrawTexture(texture_star, 5, GetScreenWidth() / 2, WHITE);
					DrawTexture(texture_star, 40, GetScreenWidth() / 2, WHITE);
					DrawTexture(texture_star, 75, GetScreenWidth() / 2, WHITE);

				}
				else if (cantstars_plvl[2] == 2) {

					DrawTexture(texture_star, 5, GetScreenWidth() / 2, WHITE);
					DrawTexture(texture_star, 40, GetScreenWidth() / 2, WHITE);
					DrawTexture(texture_starb, 75, GetScreenWidth() / 2, WHITE);

				}
				else if (cantstars_plvl[2] == 1) {
					DrawTexture(texture_star, 5, GetScreenWidth() / 2, WHITE);
					DrawTexture(texture_starb, 40, GetScreenWidth() / 2, WHITE);
					DrawTexture(texture_starb, 75, GetScreenWidth() / 2, WHITE);
				}
				else {

					DrawTexture(texture_starb, 5, GetScreenWidth() / 2, WHITE);
					DrawTexture(texture_starb, 40, GetScreenWidth() / 2, WHITE);
					DrawTexture(texture_starb, 75, GetScreenWidth() / 2, WHITE);
				}
			}
			if (level == 5) {

				DrawText("Level 5", 5, 4, 20, DARKBLUE);
				if (!gameover) {
					DrawText("Level 4 stars", 5, GetScreenWidth() / 2 - 20, 20, BLACK);

					if (cantstars_plvl[3] == 3) {
						DrawTexture(texture_star, 5, GetScreenWidth() / 2, WHITE);
						DrawTexture(texture_star, 40, GetScreenWidth() / 2, WHITE);
						DrawTexture(texture_star, 75, GetScreenWidth() / 2, WHITE);

					}
					else if (cantstars_plvl[3] == 2) {

						DrawTexture(texture_star, 5, GetScreenWidth() / 2, WHITE);
						DrawTexture(texture_star, 40, GetScreenWidth() / 2, WHITE);
						DrawTexture(texture_starb, 75, GetScreenWidth() / 2, WHITE);

					}
					else if (cantstars_plvl[3] == 1) {
						DrawTexture(texture_star, 5, GetScreenWidth() / 2, WHITE);
						DrawTexture(texture_starb, 40, GetScreenWidth() / 2, WHITE);
						DrawTexture(texture_starb, 75, GetScreenWidth() / 2, WHITE);
					}
					else {

						DrawTexture(texture_starb, 5, GetScreenWidth() / 2, WHITE);
						DrawTexture(texture_starb, 40, GetScreenWidth() / 2, WHITE);
						DrawTexture(texture_starb, 75, GetScreenWidth() / 2, WHITE);
					}
				}
			}
			if (level == 5 && gameover) {
				DrawText("Level 5 stars", 5, GetScreenWidth() / 2 - 20, 20, BLACK);

				if (cantstars_plvl[4] == 3) {
					DrawTexture(texture_star, 5, GetScreenWidth() / 2, WHITE);
					DrawTexture(texture_star, 40, GetScreenWidth() / 2, WHITE);
					DrawTexture(texture_star, 75, GetScreenWidth() / 2, WHITE);

				}
				else if (cantstars_plvl[4] == 2) {

					DrawTexture(texture_star, 5, GetScreenWidth() / 2, WHITE);
					DrawTexture(texture_star, 40, GetScreenWidth() / 2, WHITE);
					DrawTexture(texture_starb, 40, GetScreenWidth() / 2, WHITE);

				}
				else if (cantstars_plvl[4] == 1) {
					DrawTexture(texture_star, 5, GetScreenWidth() / 2, WHITE);
					DrawTexture(texture_starb, 40, GetScreenWidth() / 2, WHITE);
					DrawTexture(texture_starb, 75, GetScreenWidth() / 2, WHITE);
				}
				else {

					DrawTexture(texture_starb, 5, GetScreenWidth() / 2, WHITE);
					DrawTexture(texture_starb, 40, GetScreenWidth() / 2, WHITE);
					DrawTexture(texture_starb, 75, GetScreenWidth() / 2, WHITE);
				}
			}
			gameOver::draw();

		}

	}
}