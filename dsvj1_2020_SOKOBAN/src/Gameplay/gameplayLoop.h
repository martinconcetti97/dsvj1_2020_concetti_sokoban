#ifndef GAMEPLAY_H
#define GAMEPLAY_H

#include "raylib.h"
#include "menu.h"

namespace sokaban {
	extern bool gameover;
	extern bool pause;
	static bool muteMusicbool;
	extern int boxcounteraux, boxcounter;
	extern buttonplay return_from_g;

	namespace gameplay {
		void init();
		void input();
		void update();
		void draw();
	}
}
#endif 