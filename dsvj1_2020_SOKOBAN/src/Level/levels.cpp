#include "levels.h"

#include "gameplayLoop.h"
#include "textures.h"
#include "audio.h"

using namespace std;

namespace sokaban {
	piece p[ROWS][COLUMNS];
	char cadena[110] = "";
	int counter = 48;
	int stars_plvl[4];
	int cantstars_plvl[4];
	int level = 1;
	int aux = 0;
	float aux2 = 0;
	int steps = 0;
	int move_counter;
	
	namespace ns_levels {

		void init() {
			boxcounteraux = 0;
			boxcounter = 0;
			move_counter = 0;
			int k = 0;
			ifstream fentrada;
			if (level == 1) {
				fentrada.open("levels\\level1.txt");
			}
			else if (level == 2) {
				fentrada.open("levels\\level2.txt");
			}
			else if (level == 3) {
				fentrada.open("levels\\level3.txt");
			}
			else if (level == 4) {
				fentrada.open("levels\\level4.txt");
			}
			else if (level == 5) {
				fentrada.open("levels\\level5.txt");
			}
			while (!fentrada.eof()) {
				fentrada.getline(cadena, 110);
			}
			for (int i = 0; i < ROWS; i++)
			{
				for (int j = 0; j < COLUMNS; j++)
				{
					if (k == 0) {
						p[i][j].type = cadena[k] - counter;


					}
					else {
						p[i][j].type = cadena[k] - counter;
					}
					k++;

				}
			}
			fentrada.close();

			for (int i = 0; i < ROWS; i++)
			{
				for (int j = 0; j < COLUMNS; j++)
				{
					p[i][j].size = { 40, 40 };
					p[i][j].position = { 0 + ((GetScreenWidth() - p[i][j].size.x * ROWS) / 2) + p[i][j].size.x * i, 0 + ((GetScreenHeight() - p[i][j].size.y * ROWS) / 2) + p[i][j].size.y * j };
					p[i][j].active = true;
					p[i][j].speed = 350.0f;

					p[i][j].click_2 = false;

					if (p[i][j].type == 0) {
						p[i][j].status = 0;
					}
				}
			}

		}
		void update() {
			//std::cout << move_counter << "---";
			for (int i = 0; i < ROWS; i++)
			{
				for (int j = 0; j < COLUMNS; j++)
				{
					if (p[i][j].type == 0) {
						p[i][j].color = GREEN;
					}
					else if (p[i][j].type == 1) {
						p[i][j].color = RED;
					}
					else if (p[i][j].type == 2) {
						p[i][j].color = BLUE;
					}
					else if (p[i][j].type == 3) {
						p[i][j].color = BEIGE;

					}
					else if (p[i][j].type == 4) {
						p[i][j].color = WHITE;
					}
				}
			}
			for (int i = 0; i < ROWS; i++)
			{
				for (int j = 0; j < COLUMNS; j++)
				{
					if (p[i][j].type == 0) {
						if (IsKeyDown(KEY_RIGHT)) {

							p[i][j].status = 1;
							if (p[i + 1][j].type == 4) {

								if (steps == 0) {
									move_counter++;
									PlaySound(playermove);

									aux2 = p[i][j].position.y;
									p[i][j].position.y = p[i + 1][j].position.y;
									p[i + 1][j].position.y = aux2;

									aux = p[i][j].type;
									p[i][j].type = p[i + 1][j].type;
									p[i + 1][j].type = aux;

									steps++;
								}

							}

							if (p[i + 1][j].type == 3 && p[i + 2][j].type == 2) {

								if (p[i + 1][j].click_2 == false) {

									if (steps == 0) {
										move_counter++;

										PlaySound(playermove);

										p[i + 2][j].type = 3;
										p[i + 2][j].click_2 = true;
										p[i + 1][j].type = 0;
										p[i][j].type = 4;
										PlaySound(movebox);

										boxcounteraux = 0;
										steps++;

									}
								}
							}
							if (p[i + 1][j].type == 3 && p[i + 2][j].type != 1 && p[i + 2][j].type != 3) {
								if (p[i + 1][j].click_2 == false) {
									if (steps == 0) {
										move_counter++;

										PlaySound(playermove);

										p[i][j].position.y = p[i + 1][j].position.y;
										p[i + 1][j].position.y = p[i + 2][j].position.y;
										p[i][j].type = 4;
										p[i + 1][j].type = 0;
										p[i + 2][j].type = 3;
										steps++;
									}
								}

							}
						}
						else if (IsKeyDown(KEY_LEFT)) {


							p[i][j].status = 2;

							if (p[i - 1][j].type == 4) {
								if (steps == 0) {
									move_counter++;

									PlaySound(playermove);

									aux2 = p[i][j].position.y;
									p[i][j].position.y = p[i - 1][j].position.y;
									p[i - 1][j].position.y = aux2;

									aux = p[i][j].type;
									p[i][j].type = p[i - 1][j].type;
									p[i - 1][j].type = aux;
									steps++;

								}

							}

							if (p[i - 1][j].type == 3 && p[i - 2][j].type == 2) {

								if (p[i - 1][j].click_2 == false) {

									if (steps == 0) {
										move_counter++;

										PlaySound(playermove);

										p[i - 2][j].type = 3;
										p[i - 2][j].click_2 = true;
										p[i - 1][j].type = 0;
										p[i][j].type = 4;
										PlaySound(movebox);

										boxcounteraux = 0;
										steps++;
									}
								}
							}
							if (p[i - 1][j].type == 3 && p[i - 2][j].type != 1 && p[i - 2][j].type != 3) {
								if (p[i - 1][j].click_2 == false) {
									if (steps == 0) {
										move_counter++;

										PlaySound(playermove);

										p[i][j].position.y = p[i - 1][j].position.y;
										p[i - 1][j].position.y = p[i - 2][j].position.y;
										p[i][j].type = 4;
										p[i - 1][j].type = 0;
										p[i - 2][j].type = 3;
										steps++;
									}
								}
							}
						}
						else if (IsKeyDown(KEY_UP)) {


							p[i][j].status = 3;

							if (p[i][j - 1].type == 4) {

								if (steps == 0) {
									move_counter++;

									PlaySound(playermove);

									aux2 = p[i][j].position.x;
									p[i][j].position.x = p[i][j - 1].position.x;
									p[i][j - 1].position.x = aux2;

									aux = p[i][j].type;
									p[i][j].type = p[i][j - 1].type;
									p[i][j - 1].type = aux;
									steps++;
								}

							}

							if (p[i][j - 1].type == 3 && p[i][j - 2].type == 2) {

								if (p[i][j - 1].click_2 == false) {

									if (steps == 0) {
										move_counter++;

										PlaySound(playermove);

										p[i][j - 2].type = 3;
										p[i][j - 2].click_2 = true;
										p[i][j - 1].type = 0;
										p[i][j].type = 4;
										PlaySound(movebox);

										boxcounteraux = 0;
										steps++;
									}
								}
							}
							if (p[i][j - 1].type == 3 && p[i][j - 2].type != 1 && p[i][j - 2].type != 3) {
								if (p[i][j - 1].click_2 == false) {
									if (steps == 0) {
										move_counter++;

										PlaySound(playermove);

										p[i][j].position.x = p[i][j - 1].position.x;
										p[i][j - 1].position.x = p[i][j - 2].position.x;
										p[i][j].type = 4;
										p[i][j - 1].type = 0;
										p[i][j - 2].type = 3;
										steps++;
									}
								}
							}
						}
						else if (IsKeyDown(KEY_DOWN)) {


							p[i][j].status = 4;

							if (p[i][j + 1].type == 4) {

								if (steps == 0) {
									move_counter++;

									PlaySound(playermove);

									aux2 = p[i][j].position.x;
									p[i][j].position.x = p[i][j + 1].position.x;
									p[i][j + 1].position.x = aux2;

									aux = p[i][j].type;
									p[i][j].type = p[i][j + 1].type;
									p[i][j + 1].type = aux;
									steps++;
								}

							}


							if (p[i][j + 1].type == 3 && p[i][j + 2].type == 2) {

								if (p[i][j + 1].click_2 == false) {

									if (steps == 0) {
										move_counter++;

										PlaySound(playermove);

										p[i][j + 2].type = 3;
										p[i][j + 2].click_2 = true;
										p[i][j + 1].type = 0;
										p[i][j].type = 4;
										PlaySound(movebox);

										boxcounteraux = 0;
										steps++;
									}
								}
							}
							if (p[i][j + 1].type == 3 && p[i][j + 2].type != 1 && p[i][j + 2].type != 3) {
								if (p[i][j + 1].click_2 == false) {
									if (steps == 0) {
										move_counter++;

										PlaySound(playermove);

										p[i][j].position.x = p[i][j + 1].position.x;
										p[i][j + 1].position.x = p[i][j + 2].position.x;
										p[i][j].type = 4;
										p[i][j + 1].type = 0;
										p[i][j + 2].type = 3;
										steps++;
									}
								}
							}
						}
						else
							steps = 0;
					}

				}

			}
		}
		void draw() {
			for (int i = 0; i < ROWS; i++)
			{
				for (int j = 0; j < COLUMNS; j++)
				{

					if (p[i][j].active)
					{
						if (p[i][j].type == 0) {
							if (p[i][j].status == 1) {
								DrawTexture(texture_pr, static_cast<int>(p[i][j].position.x), static_cast<int>(p[i][j].position.y), WHITE);

							}
							else if (p[i][j].status == 2) {
								DrawTexture(texture_pl, static_cast<int>(p[i][j].position.x), static_cast<int>(p[i][j].position.y), WHITE);

							}
							else if (p[i][j].status == 3) {
								DrawTexture(texture_pb, static_cast<int>(p[i][j].position.x), static_cast<int>(p[i][j].position.y), WHITE);

							}
							else if (p[i][j].status == 4) {
								DrawTexture(texture_pf, static_cast<int>(p[i][j].position.x), static_cast<int>(p[i][j].position.y), WHITE);

							}
							else if (p[i][j].status == 0) {
								DrawTexture(texture_pf, static_cast<int>(p[i][j].position.x), static_cast<int>(p[i][j].position.y), WHITE);

							}

						}
						else if (p[i][j].type == 1) {
							DrawTexture(texture_b1, static_cast<int>(p[i][j].position.x), static_cast<int>(p[i][j].position.y), WHITE);
						}
						else if (p[i][j].type == 2) {
							DrawTexture(texture_b2, static_cast<int>(p[i][j].position.x), static_cast<int>(p[i][j].position.y), WHITE);

						}
						else if (p[i][j].type == 3) {
							if (p[i][j].click_2) {
								DrawTexture(texture_b3_1, static_cast<int>(p[i][j].position.x), static_cast<int>(p[i][j].position.y), WHITE);

							}
							else if (!p[i][j].click_2) {
								DrawTexture(texture_b3, static_cast<int>(p[i][j].position.x), static_cast<int>(p[i][j].position.y), WHITE);
							}

						}
						else if (p[i][j].type == 4) {
							DrawTexture(texture_ground, static_cast<int>(p[i][j].position.x), static_cast<int>(p[i][j].position.y), WHITE);

						}
#if DEBUG
						//	DrawText(FormatText("%i", p[i][j].type), static_cast<int>(p[i][j].position.x + 15), static_cast<int>(p[i][j].position.y + 15), 5, BLACK);
#endif				
					}

				}
			}
		}
	}
}
