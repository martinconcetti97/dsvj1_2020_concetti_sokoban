#ifndef LEVEL_H
#define LEVEL_H

#include <cstdlib>
#include <ctime> 
#include <iostream>
#include <fstream>

#include "raylib.h"

namespace sokaban {
	const int ROWS = 10;
	const int COLUMNS = 10;

	struct piece {
		Vector2 position;
		Vector2 size;
		bool active;
		int type;
		Color color;
		float speed;
		bool click_2;
		int status;

	};
	extern piece p[ROWS][COLUMNS];
	extern int stars_plvl[4];
	extern int cantstars_plvl[4];

	extern int level;
	extern int move_counter;

	namespace ns_levels {
		void init();
		void update();
		void draw();

	}

}
#endif
#pragma once
