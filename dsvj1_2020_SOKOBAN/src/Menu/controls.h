#ifndef INSTRUCTIONS_H
#define INSTRUCTIONS_H
#include "menu.h"

namespace sokaban {
	extern buttonplay return_from_i;

	namespace inst_s
	{
		void init();
		void input();
		void update();
		void draw();
	}
}
#endif 

