#include "credits.h"

#include "gameLoop.h"
#include "audio.h"

namespace sokaban{
	buttonplay return_from_c;
	namespace credits_s
	{
		void init() {
			return_from_c.frameHeight = texture_button.height / NUM_FRAMES;
			return_from_c.sourceRec = { 0, 0, static_cast<float>(texture_button.width), static_cast<float>(return_from_c.frameHeight) };
			return_from_c.btnBounds = { static_cast<float>(GetScreenWidth() / 2 - texture_button.width / 2),
					static_cast<float>(GetScreenHeight() / 1 - 40 - texture_button.height / NUM_FRAMES / 2),  static_cast<float>(texture_button.width), static_cast<float>(return_from_c.frameHeight) };
			return_from_c.btnAction = false;
			return_from_c.btnState = 0;

			return_from_c.mousePoint = { 0.0f, 0.0f };

		}
		void input()
		{
			return_from_c.mousePoint = GetMousePosition();
			return_from_c.btnAction = false;

			if (CheckCollisionPointRec(return_from_c.mousePoint, return_from_c.btnBounds))
			{
				if (IsMouseButtonDown(MOUSE_LEFT_BUTTON)) {
					return_from_c.btnState = 2;

				}
				else {
					return_from_c.btnState = 1;
				}

				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)) {
					return_from_c.btnAction = true;
					screen = 0;
					PlaySound(click);

				}

			}
			else {
				return_from_c.btnState = 0;
			}

			return_from_c.sourceRec.y = static_cast<float>(return_from_c.btnState * return_from_c.frameHeight);
		}
		void update() {
			UpdateMusicStream(menu_song);

			credits_s::input();
		}
		void draw()
		{

			DrawTexture(texture_backcredits, 0, 0, WHITE);

			DrawTextureRec(texture_return, return_from_c.sourceRec, { return_from_c.btnBounds.x, return_from_c.btnBounds.y }, WHITE);

		}
	}
}