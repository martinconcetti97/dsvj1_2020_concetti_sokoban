#ifndef CREDITS_H
#define CREDITS_H

#include "menu.h"

namespace sokaban {

	extern buttonplay return_from_c;

	namespace credits_s
	{
		void init();
		void input();
		void update();
		void draw();
	}
}
#endif 
