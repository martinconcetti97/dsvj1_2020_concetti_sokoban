#include "gameOver.h"

#include "gameplayLoop.h"
#include "levels.h"
#include "audio.h"

namespace sokaban {
	int ps_1 = 0;

	namespace gameOver {
		void input() {
			if (gameover) {
				if (ps_1 == 0) {
					PlaySound(wingame_s);
					ps_1++;
				}

				if (IsKeyPressed(KEY_ENTER)) {
					ps_1 = 0;
					gameover = false;
					level = 1;
					gameplay::init();
				}
			}
		}
		void draw() {
			if (gameover) {
				DrawText("You win", GetScreenWidth() / 2 - (MeasureText("You win", letterSize) / 2), GetScreenHeight() / 4 + letterSize, letterSize, BLACK);

				DrawText("Press ENTER to play again", GetScreenWidth() / 2 - (MeasureText("Press ENTER to play again", letterSize) / 2), GetScreenHeight() / 2 + letterSize, letterSize, BLACK);

			}
		}
	}
}