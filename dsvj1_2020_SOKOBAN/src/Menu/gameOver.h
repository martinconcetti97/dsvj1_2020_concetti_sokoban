#ifndef GAMEOVER_H
#define GAMEOVER_H

namespace sokaban {
	namespace gameOver {
		void input();
		void draw();
	}
}
#endif 