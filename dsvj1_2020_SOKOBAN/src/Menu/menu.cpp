#include "menu.h"

#include "gameLoop.h"
#include "gameplayLoop.h"
#include "textures.h"
#include "audio.h"
#include <iostream>
using namespace std;

namespace sokaban {
	static Rectangle rect1;
	Vector2 mause;

	buttonplay bplay[4];
	int numPosition = 0;
	int randomPos;
	namespace menu {
		void init() {
			
			rect1.height = 50;
			rect1.width = 100;
			rect1.x =  0;
			rect1.y = 0;
			LoadTextures();
			for (int i = 0; i < 4; i++) {
				if (i == 0) {
					numPosition = 6;
					randomPos = 0;
				}
				else if (i == 1) {
					numPosition = 3;
					randomPos = 0;

				}
				else if (i == 2) {
					numPosition = 2;
					randomPos = 0;
				}
				else if (i == 3) {
					numPosition = 2;
					randomPos = 75;

				}
				bplay[i].frameHeight = texture_button.height / NUM_FRAMES;
				bplay[i].sourceRec = { 0, 0, static_cast<float>(texture_button.width), static_cast<float>(bplay[i].frameHeight) };
				bplay[i].btnBounds = { static_cast<float>(screenWidth / 2 - texture_button.width / 2),
					static_cast<float>(screenHeight / numPosition + randomPos - texture_button.height / NUM_FRAMES / 2),  static_cast<float>(texture_button.width), static_cast<float>(bplay[i].frameHeight) };
				bplay[i].btnAction = false;
				bplay[i].btnState = 0;

				bplay[i].mousePoint = { 0.0f, 0.0f };
			}

		}
		void input() {

			for (int i = 0; i < 4; i++) {

				bplay[i].mousePoint = GetMousePosition();
				bplay[i].btnAction = false;

				if (CheckCollisionPointRec(bplay[i].mousePoint,rect1))
				{
					if (IsMouseButtonDown(MOUSE_LEFT_BUTTON)) {
						inGame = false;
					}
				}
				if (CheckCollisionPointRec(bplay[i].mousePoint, bplay[i].btnBounds))
				{
					if (IsMouseButtonDown(MOUSE_LEFT_BUTTON)) {
						bplay[i].btnState = 2;

					}
					else {
						bplay[i].btnState = 1;
					}

					if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)) {
						bplay[i].btnAction = true;
						if (i == 0) {
							gameplay::init();
							screen = 1;
						}
						else if (i == 1) {
							screen = 2;
						}
						else if (i == 2) {
							screen = 3;
						}
						else if (i == 3) {
							screen = 4;
						}
						PlaySound(click);


					}

				}
				else {
					bplay[i].btnState = 0;
				}

				bplay[i].sourceRec.y = static_cast<float>(bplay[i].btnState * bplay[i].frameHeight);
			}

		}
		void update() {
			UpdateMusicStream(menu_song);

			menu::input();
		}

		void draw() {
			DrawTexture(texture_background, GetScreenWidth() / 2 - texture_background.width / 2, GetScreenHeight() / 2 - texture_background.height / 2, WHITE);
			DrawText("version 1.0", 10, GetScreenHeight() - 35, letterSize, SKYBLUE);
			DrawRectangle(static_cast<int>(rect1.x), static_cast<int>(rect1.y), static_cast<int>(rect1.width), static_cast<int>(rect1.height), BLACK);

			DrawText("Close", static_cast<int>(rect1.x) + 18, static_cast<int>(rect1.y) + 10, 20, WHITE);


			for (int i = 0; i < 4; i++) {
				if (i == 0) {
					DrawTextureRec(texture_button, bplay[i].sourceRec, { bplay[i].btnBounds.x, bplay[i].btnBounds.y }, WHITE);
				}
				else if (i == 1) {
					DrawTextureRec(texture_button1, bplay[i].sourceRec, { bplay[i].btnBounds.x, bplay[i].btnBounds.y }, WHITE);

				}
				else if (i == 2) {
					DrawTextureRec(texture_button2, bplay[i].sourceRec, { bplay[i].btnBounds.x, bplay[i].btnBounds.y }, WHITE);

				}
				else if (i == 3) {
					DrawTextureRec(texture_button3, bplay[i].sourceRec, { bplay[i].btnBounds.x, bplay[i].btnBounds.y }, WHITE);

				}

			}
		}
	}
}