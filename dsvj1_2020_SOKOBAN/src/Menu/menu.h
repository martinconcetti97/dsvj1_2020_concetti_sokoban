#ifndef MENU_H
#define MENU_H
#include "textures.h"
#include "gameLoop.h"

namespace sokaban {
	struct buttonplay {
		int frameHeight;
		Rectangle sourceRec;
		Rectangle btnBounds;
		bool btnAction;
		Vector2 mousePoint;
		int btnState;
	};
	const int NUM_FRAMES = 3;
	extern buttonplay bplay[4];
	namespace menu {

		void init();
		void input();
		void update();
		void draw();
	}
}
#endif
