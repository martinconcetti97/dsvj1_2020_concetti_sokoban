#ifndef RESOLUTION_H
#define RESOLUTION_H
#include "menu.h"
namespace sokaban{
	struct buttonplay2 {
		int frameHeight;
		Rectangle sourceRec;
		Rectangle btnBounds;
		bool btnAction;
		Vector2 mousePoint;
		int btnState;
	};

	namespace resolution_s
	{

		extern int resolution_v;
		extern bool change_r;

		extern buttonplay2 bsettings[3];
		void init();
		void input();
		void update();
		void draw();
	}
}
#endif 
