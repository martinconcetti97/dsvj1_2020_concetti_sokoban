#include "textures.h"

namespace sokaban {

	//buttons image
	static Image iButton;
	static Image iButton1;
	static Image iButton2;
	static Image iButton3;
	static Image iReturn;

	static Image iSliderR;
	static Image iSliderL;

	//screen - 1 image
	static Image iBackground;
	static Image iBcredits;
	static Image iNstruccions;

	static Image ib1;
	static Image ib3;
	static Image ib3_1;
	static Image ib2;
	static Image iground;

	static Image ipr;
	static Image ipl;
	static Image ipf;
	static Image ipb;
	static Image iStar;
	static Image iStarb;

	//buttons textures
	Texture2D texture_button;
	Texture2D texture_button1;
	Texture2D texture_button2;
	Texture2D texture_button3;
	Texture2D texture_return;

	Texture2D texture_slider_right;
	Texture2D texture_slider_left;

	//screen - 1 textures
	Texture2D texture_background;
	Texture2D texture_backcredits;
	Texture2D texture_instruccions;

	Texture2D texture_b1;
	Texture2D texture_b3;
	Texture2D texture_b3_1;
	Texture2D texture_b2;
	Texture2D texture_ground;

	Texture2D texture_pr;
	Texture2D texture_pl;
	Texture2D texture_pf;
	Texture2D texture_pb;

	Texture2D texture_star;
	Texture2D texture_starb;

	void LoadTextures()
	{

		// buttons load image
		iButton = LoadImage("res/assets/play.png");
		iButton1 = LoadImage("res/assets/credits.png");
		iButton2 = LoadImage("res/assets/i_button.png");
		iButton3 = LoadImage("res/assets/resolution.png");
		iReturn = LoadImage("res/assets/return.png");

		iSliderR = LoadImage("res/assets/blue_sliderRight.png");
		iSliderL = LoadImage("res/assets/blue_sliderLeft.png");

		// screens 1 load image
		iBackground = LoadImage("res/assets/background.png");
		iBcredits = LoadImage("res/assets/imagen_credits.png");
		iNstruccions = LoadImage("res/assets/controls_screen.png");

		iStar = LoadImage("res/assets/star.png");
		iStarb = LoadImage("res/assets/star_b.png");

		ib1 = LoadImage("res/assets/1.png");
		ib2 = LoadImage("res/assets/2.png");
		ib3 = LoadImage("res/assets/3_0.png");
		ib3_1 = LoadImage("res/assets/3_1.png");
		iground = LoadImage("res/assets/ground.png");

		ipf = LoadImage("res/assets/player_front.png");
		ipb = LoadImage("res/assets/player_back.png");
		ipl = LoadImage("res/assets/player_left.png");
		ipr = LoadImage("res/assets/player_right.png");

		ImageResize(&ib1, 40, 40);
		ImageResize(&ib2, 40, 40);
		ImageResize(&ib3, 40, 40);
		ImageResize(&ib3_1, 40, 40);
		ImageResize(&iground, 40, 40);

		ImageResize(&ipf, 40, 40);
		ImageResize(&ipb, 40, 40);
		ImageResize(&ipl, 40, 40);
		ImageResize(&ipr, 40, 40);

		ImageResize(&iBackground, GetScreenWidth(), GetScreenHeight());
		ImageResize(&iBcredits, GetScreenWidth(), GetScreenHeight());
		ImageResize(&iNstruccions, GetScreenWidth(), GetScreenHeight());


		texture_button = LoadTextureFromImage(iButton);
		texture_button1 = LoadTextureFromImage(iButton1);
		texture_button2 = LoadTextureFromImage(iButton2);
		texture_button3 = LoadTextureFromImage(iButton3);
		texture_return = LoadTextureFromImage(iReturn);

		texture_star = LoadTextureFromImage(iStar);
		texture_starb = LoadTextureFromImage(iStarb);

		texture_slider_right = LoadTextureFromImage(iSliderR);
		texture_slider_left = LoadTextureFromImage(iSliderL);

		texture_background = LoadTextureFromImage(iBackground);
		texture_backcredits = LoadTextureFromImage(iBcredits);
		texture_instruccions = LoadTextureFromImage(iNstruccions);

		texture_b1 = LoadTextureFromImage(ib1);
		texture_b2 = LoadTextureFromImage(ib2);
		texture_b3 = LoadTextureFromImage(ib3);
		texture_b3_1 = LoadTextureFromImage(ib3_1);
		texture_ground = LoadTextureFromImage(iground);

		texture_pf = LoadTextureFromImage(ipf);
		texture_pb = LoadTextureFromImage(ipb);
		texture_pl = LoadTextureFromImage(ipl);
		texture_pr = LoadTextureFromImage(ipr);

		UnloadImage(iBackground);
		UnloadImage(iBcredits);
		UnloadImage(iNstruccions);

		UnloadImage(iButton);
		UnloadImage(iButton1);
		UnloadImage(iButton2);
		UnloadImage(iButton3);
		UnloadImage(iReturn);

		UnloadImage(ib1);
		UnloadImage(ib2);
		UnloadImage(ib3);
		UnloadImage(ib3_1);
		UnloadImage(iground);

		UnloadImage(iSliderL);
		UnloadImage(iSliderR);

		UnloadImage(ipf);
		UnloadImage(ipb);
		UnloadImage(ipl);
		UnloadImage(ipr);

		UnloadImage(iStar);
		UnloadImage(iStarb);

	}
}