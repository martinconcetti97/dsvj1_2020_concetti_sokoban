#ifndef TEXTURES_H
#define TEXTURES_H

#include "raylib.h"

namespace sokaban {
	//buttons
	extern Texture2D texture_button;
	extern Texture2D texture_button1;
	extern Texture2D texture_button2;
	extern Texture2D texture_button3;
	extern Texture2D texture_return;


	extern Texture2D texture_slider_right;
	extern Texture2D texture_slider_left;

	//screens - 1
	extern Texture2D texture_background;
	extern Texture2D texture_backcredits;
	extern Texture2D texture_instruccions;

	//gameplay
	extern Texture2D texture_b1;
	extern Texture2D texture_b3;
	extern Texture2D texture_b3_1;
	extern Texture2D texture_b2;
	extern Texture2D texture_ground;

	extern Texture2D texture_pr;
	extern Texture2D texture_pl;
	extern Texture2D texture_pf;
	extern Texture2D texture_pb;

	extern Texture2D texture_star;
	extern Texture2D texture_starb;

	void LoadTextures();
}
#endif
