#ifndef GAME_H
#define GAME_H

#include "raylib.h"

namespace sokaban {

	extern int screenWidth;
	extern int screenHeight;
	extern int screen;
	extern int letterSize;
	extern bool inGame;

	void execute();
}
#endif